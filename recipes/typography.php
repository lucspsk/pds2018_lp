<!doctype html>
<html lang="en-US">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="images/favicon.png"/>

    <!-- Font Files -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

    <title>Mesa Posta</title>

    <!-- Stylesheets -->
     <link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
    <link rel='stylesheet' id='nivoslider-css' href='js/nivo-slider/nivo-slider.css?ver=4.1.1' type='text/css' media='all'/>
    <link rel='stylesheet' id='prettyPhoto-css' href='js/prettyPhoto/css/prettyPhoto.css?ver=4.1.1' type='text/css' media='all'/>
    <link rel='stylesheet' id='plupload_css-css' href='js/jquery.ui.plupload/css/jquery.ui.plupload.css?ver=4.1.1' type='text/css' media='all'/>
    <link rel='stylesheet' id='select2-css' href='css/select2.css?ver=4.1.1' type='text/css' media='all'/>
    <link rel='stylesheet' id='font-awesome-css' href='css/font-awesome.min.css?ver=4.1.1' type='text/css' media='all'/>
    <link rel="stylesheet" href="style.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="skin/orange/orange.css" type="text/css" media="all"/>
    <link rel='stylesheet' id='responsive-css' href='responsive.css?ver=4.1.1' type='text/css' media='all'/>

</head>

<body>

<!-- ============= HEADER STARTS HERE ============== -->
<div id="header-wrapper" class="clearfix">
    <div id="header" class="clearfix">
        <!-- WEBSITE LOGO -->
        <a class="responsive_logo" href="index.php"><img src="images/logo.png" alt="" class="logo"/></a>
        <a href="index.php"><h1 class="sitenametext">Mesa Posta</h1></a>
        <a href="index.php"><img class="header-img" src="images/header-image.png" alt="Mesa Posta"/></a>
    </div>
    <!-- end of header div -->

    <span class="w-pet-border"></span>

    <!-- NAVIGATION BAR STARTS HERE -->
    <div id="nav-wrap">
        <div class="inn-nav clearfix">

            <!-- MAIN NAVIGATION STARTS HERE -->
            <ul id="menu-main-navigation" class="nav">
                <li><a href="index.php">Home</a></li>
                <li><a href="recipes-listing.php">Recipe listing</a>
                    <ul class="sub-menu">
                        <li><a href="recipe-single-1.php">Recipe Single one</a></li>
                        <li><a href="recipe-single-2.php">Recipe Single two</a></li>
                    </ul>
                </li>
				<li><a href="advance-search.php">Advanced Search</a></li>
                <li>
                    <a href="account.php">My Account</a>
					<?php include('session.php'); check_login(); if(isset($_SESSION['login_user'])){ ?>
                    <ul class="sub-menu">
                        <li><a href="author.php">Profile</a></li>
                        <li><a href="typography.php">Favorites</a></li>
						<li style="color: white;">---------------------------</li>
						<li><a href="logout.php">Logout</a></li>
                    </ul>
					<?php } else if(!isset($_SESSION['login_user'])) { ?>
					<ul class="sub-menu">
                        <li><a href="login.php">You are not logged in</a></li>
                    </ul>
					<?php } ?>
                </li>
				<li><a href="contact.html">Contact</a></li>
				<li><a href="about-us.php">About</a></li>
                
            </ul>
            <!-- MAIN NAVIGATION ENDS HERE -->


            <!-- SOCIAL NAVIGATION -->
            <ul id="menu-social-menu" class="social-nav">
                <li class="facebook"><a href="#"></a></li>
                <li class="twitter"><a href="#"></a></li>
                <li class="rss"><a href="#"></a></li>
                <li class="flickr"><a href="#"></a></li>
            </ul>
        </div>
    </div>
    <!-- end of nav-wrap -->
    <!-- NAVIGATION BAR ENDS HERE -->

</div>
<!-- end of header-wrapper div -->

<!-- ============= HEADER ENDS HERE ============== -->


<!-- ============= CONTAINER STARTS HERE ============== -->
<div class="main-wrap">
    <div id="container">
        <!-- WEBSITE SEARCH STARTS HERE -->

        <div class="top-search  clearfix">
            <h3 class="head-pet"><span>Recipe Search</span></h3>

            <form action="#" id="searchform">
                <p>
                    <input type="text" name="s" id="s" class="field" value="" placeholder="Search for"/>
                    <input type="submit" name="s_submit" id="s-submit" value=""/>
                </p>
            </form>

            <p class="statement"><span class="fireRed">Recipe Types:</span>
                <a href="#">Beef</a> ,
                <a href="#">Cheese</a> ,
                <a href="#">Chicken</a> ,
                <a href="#">Chocolate</a> ,
                <a href="#">Fish</a> ,
                <a href="#">Pizzas</a>,
                <a href="#">Potatos</a>,
                <a href="#">Rolls</a>
            </p>

        </div>
        <!-- end of top-search div-->


        <!-- ============= CONTENT AREA STARTS HERE ============== -->
        <div id="content" class="clearfix ">
            <div id="left-area" class="clearfix full-wide">

                <div class="post-77 page type-page status-publish hentry" id="page-77">
                    <h2 class="post-title w-bot-border">Typography</h2><br/>

                    <p class="meta"><span class="comments"><a href="#" title="Comment on Typography">0 Comments</a></span>
                        <span>|</span> Last Update: <span> March 6, 2012</span>
                    </p>

                    <h1>Sweet maple and a layer of nuts in this buttery dessert makes nuts in this buttery dessert makes Christmas oh so special!</h1>
                    <hr/>
                    <p>&nbsp;</p>

                    <h2>Sweet maple and a layer of nuts in this buttery dessert makes nuts in this buttery dessert makes Christmas oh so special!</h2>
                    <hr/>
                    <p>&nbsp;</p>

                    <h3>Sweet maple and a layer of nuts in this buttery dessert makes nuts in this buttery dessert makes Christmas oh so special!</h3>
                    <hr/>
                    <p>&nbsp;</p>
                    <h4>Sweet maple and a layer of nuts in this buttery dessert makes nuts in this buttery dessert makes Christmas oh so special!</h4>
                    <hr/>
                    <p>&nbsp;</p>
                    <h5>Sweet maple and a layer of nuts in this buttery dessert makes nuts in this buttery dessert makes Christmas oh so special!</h5>
                    <hr/>
                    <p>&nbsp;</p>

                    <h3 class="blue">Columns</h3>
                    <span class="w-pet-border"></span>

                    <div class="columns">
                        <div class="one-third">
                            <h3 class="red-heading">One Third</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                        <div class="one-third">
                            <h3 class="red-heading">One Third</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                        <div class="one-third">
                            <h3 class="red-heading">One Third</h3><span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="two-third"><h3 class="red-heading">Two Third</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg. Place flour, sugar and butter
                            in a food prssor. Process until mixture resembles fine breumbs. Process until resemble s
                            fine brcrum mixture resemble rocess until mixture resemble s fine brcrum Add egg..<br/>
                        </div>
                        <div class="one-third"><h3 class="red-heading">One Third</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                    </div>


                    <div class="columns">
                        <div class="one-fourth"><h3 class="red-heading">One Fourth</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                        <div class="one-fourth"><h3 class="red-heading">One Fourth</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                        <div class="one-fourth"><h3 class="red-heading">One Fourth</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                        <div class="one-fourth"><h3 class="red-heading">One Fourth</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                    </div>
                    <br/>

                    <div class="columns">
                        <div class="one-fourth">
                            <h3 class="red-heading">One Fourth</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                        <div class="three-fourth">
                            <h3 class="red-heading">Three Fourth</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg. Place flour, sugar and butter
                            in a food prssor. Process until mixture resembles fine breumbs. Process until mixture
                            resembles fine brcrum Add egg etc. Process until mixture resembles fine brcrum Add egg etc.
                            Process until mixture resembles fine brcrum Add egg etc. Process until mixture resembles
                            fine brcrum Add egg etc.<br/>
                        </div>

                        <div class="two-cols">
                            <h3 class="red-heading">Two Column</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg. Place flour, sugar and butter
                            in a food prssor. Process until mixture until mixture resembles resembles fine breumbs.
                            Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                        <div class="one-fourth">
                            <h3 class="red-heading">One Fourth</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                        <div class="one-fourth">
                            <h3 class="red-heading">One Fourth</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine fine
                            breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                    </div>

                    <div class="columns">
                        <div class="two-cols">
                            <h3 class="red-heading">Two Column</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg. Place flour, sugar and butter
                            in a food prssor. Process until mixture resembles fine breumbs. Process until mixture
                            resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                        <div class="two-cols">
                            <h3 class="red-heading">Two Column</h3>
                            <span class="w-pet-border"></span><br/>
                            Place flour, sugar and butter in a food prssor. Process until mixture resembles fine
                            breumbs. Process until mixture resembles fine brcrum Add egg. Place flour, sugar and butter
                            in a food prssor. Process until mixture resembles fine breumbs. Process until mixture
                            resembles fine brcrum Add egg&#8230;<br/>
                        </div>
                    </div>
                    <div class="single-col">
                        <h3 class="red-heading">Single Column</h3>
                        <span class="w-pet-border"></span>

                        <p>Place flour, sugar and butter in a food prssor. Process until mixture resembles fine breumbs.
                            Process until mixture resembles fine brcrum Add egg. Place flour, sugar and butter in a food
                            prssor. Process until mixture resembles fine breumbs. Process until mixture resembles fine
                            brcrum Add egg. Place flour, sugar and butter in a food prssor. Process until mixture
                            resembles fine breumbs. Process until mixture resembles fine brcrum Add egg&#8230;<br/>
                        </p>
                    </div>
                    <h3 class="blue">Block Quotes</h3>
                    <span class="w-pet-border"></span>

                    <p>
                        Although starting a prototype on a computer is sometimes easier, it’s not the best way to
                        visually problem-solve. Whenyou need to ideate website layouts or mobile applications or..
                    </p>
                    <blockquote class="leftalign">
                        <p>Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit sed pharetra aliquet metus the width should be&#8230;
                        </p>
                    </blockquote>
                    <p>Lonsectetur adipisicing elit, sed do eiusmod temporityst ut labore et dolore magn Praesent
                        dapibus, neque id aucibus. Lonsectetur adipisicing elit, sed do eiusmod incididunt ut labore et
                        dolore magn neque id aucibus. Lonsectetur adipisicing elit, sed do eiusmod incididunt ut labore
                        et dolore magn neque id aucibus. Lonsectetur adipisicing elit, sed do eiusmod incididunt ut
                        labore et dolore magn Praesent dapibus, id cursus faucibus.
                    </p>

                    <p>Lonsectetur adipisicing elit, sed do eiusmod temporityst ut labore et dolore magn Praesent
                        dapibus labore et dolore magn Praesent dapibus labore et dolore magn Praesent dapibus.</p>
                    <blockquote class="rightalign">
                        <p>Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit sed pharetra aliquet metus the width should be&#8230;
                        </p>
                    </blockquote>
                    <p>
                        Lonsectetur adipisicing elit, sed do eiusmod temporityst ut labore et dolore magn Praesent
                        dapibus, neque id aucibus. Lonsectetur adipisicing elit, sed do eiusmod incididunt ut labore et
                        dolore magn neque id aucibus. Lonsectetur adipisicing elit, sed do eiusmod incididunt ut labore
                        et dolore magn neque id aucibus. Lonsectetur adipisicing elit, sed do eiusmod incididunt ut
                        labore et dolore magn Praesent dapibus, id cursus faucibus.
                    </p>

                    <p>
                        Lonsectetur adipisicing elit, sed do eiusmod temporityst ut labore et dolore magn Praesent
                        dapibus labore et dolore magn Praesent dapibus labore et dolore magn Praesent dapibus.
                    </p>
                    <blockquote class="rightalign">
                        <p>Lorem ipsum dolor sit amet, consect etur
                            adipiscing elit sed pharetra aliquet metus the width consec tetur adipiscing elit sed pharetra
                            aliquet metus the width should be&#8230;.
                            <span class="end-quote"></span>
                        </p>
                    </blockquote>
                    <p>Phasellus arcu pid non! Egestas eu habitasse habitasse cum a nisi massa dis rhoncus, urna urna!
                        Aenean nunc risus natoque tempor, lacus proin vut. Sed nunc. Phasellus sed? Dignissim et
                        nascetur ultrices! Ut tincidunt cursus ultricies? Ut mus turpis ut cum porta dignissim aenean
                        pulvinar purus elementum nunc porttitor rhoncus porttitor non, aliquam.
                    </p>

                    <p>Sed nec scelerisque natoque augue enim cras urna odio placerat, elementum diam, eros a, mus, in
                        ac enim? Pid ac placerat sociis aliquet phasellus platea amet, cum porta elit lacus, ac a
                        ultrices mid diam adipiscing, pellentesque scelerisque pellentesque nisi cursus magna mattis
                        cras magna porta montes velit sed in in, sagittis.
                    </p>

                    <h3 class="blue">Custom Menus</h3>
                    <span class="w-pet-border"></span>

                    <div class="columns">
                        <div class="one-fourth">
                            <h3 class="red-heading">Sub Menu</h3>
                            <span class="w-pet-border"></span><br/>

                            <div class="menu-list">
                                <ul>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Services with Sidebar</a></li>
                                    <li><a href="#">Pricing Tables</a></li>
                                    <li><a href="#">Full Width Page</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                                <p></div>
                            <br/>
                        </div>
                        <div class="one-fourth">
                            <h3 class="red-heading">Sub Menu</h3>
                            <span class="w-pet-border"></span><br/>
                            <div class="menu-list">
                                <ul>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Services with Sidebar</a></li>
                                    <li><a href="#">Pricing Tables</a></li>
                                    <li><a href="#">Full Width Page</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                                <p>
                            </div>
                            <br/>
                        </div>
                        <div class="one-fourth">
                            <h3 class="red-heading">Sub Menu</h3>
                            <span class="w-pet-border"></span><br/>

                            <div class="menu-list">
                                <ul>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Services with Sidebar</a></li>
                                    <li><a href="#">Pricing Tables</a></li>
                                    <li><a href="#">Full Width Page</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                                <p></div>
                            <br/>
                        </div>
                        <div class="one-fourth">
                            <h3 class="red-heading">Sub Menu</h3>
                            <span class="w-pet-border"></span><br/>

                            <div class="menu-list">
                                <ul>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Services with Sidebar</a></li>
                                    <li><a href="#">Pricing Tables</a></li>
                                    <li><a href="#">Full Width Page</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                                <p></div>
                            <br/>
                        </div>
                    </div>
                </div>
                <!-- end of post div -->

                <div class="comments">

                    <!-- You can start editing here. -->
                    <!-- If comments are open, but there are no comments. -->

                    <div id="respond" class="comment-respond">
                        <h3 id="reply-title" class="comment-reply-title">Leave a Reply
                            <small>
                                <a rel="nofollow" id="cancel-comment-reply-link" href="#">Cancel reply</a>
                            </small>
                        </h3>
                        <form action="#" method="post" id="commentform" class="comment-form">
                            <p class="logged-in-as">Logged in as <a href="#">admin</a>. <a href="#" title="Log out of this account">Log out?</a></p>

                            <p class="comment-form-comment">
                                <label for="comment">Comment</label>
                                <textarea id="comment"  name="comment" cols="45"  rows="8" aria-describedby="form-allowed-tags" aria-required="true"></textarea>
                            </p>

                            <p class="form-allowed-tags" id="form-allowed-tags">
                                You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: <code>&lt;a href=&quot;&quot;
                                title=&quot;&quot;&gt; &lt;abbr title=&quot;&quot;&gt; &lt;acronym title=&quot;&quot;&gt;
                                &lt;b&gt; &lt;blockquote cite=&quot;&quot;&gt; &lt;cite&gt; &lt;code&gt; &lt;del
                                datetime=&quot;&quot;&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=&quot;&quot;&gt; &lt;strike&gt;
                                &lt;strong&gt; </code></p>

                            <p class="form-submit">
                                <input name="submit" type="submit" id="submit" class="submit" value="Post Comment"/>
                                <input type='hidden' name='comment_post_ID' value='77' id='comment_post_ID'/>
                                <input type='hidden' name='comment_parent' id='comment_parent' value='0'/>
                            </p>

                        </form>
                    </div>
                    <!-- #respond -->
                </div>
                <!-- end of comments div -->

            </div>
            <!-- end of left-area -->
            <!-- LEFT AREA ENDS HERE -->

        </div>
        <!-- end of content div -->
        <div class="bot-ads-area">
            <div class="ads-642x79">
                <a href="#"><img src="images/ad-652x95.png" alt="Recipe Ads"/></a>
            </div>
        </div>
        <!-- CONTENT ENDS HERE -->

    </div>
    <!-- end of container div -->
</div>
<div class="w-pet-border"></div>
<!-- ============= CONTAINER AREA ENDS HERE ============== -->


<!-- ============= BOTTOM AREA STARTS HERE ============== -->
<div id="bottom-wrap">
    <ul id="bottom" class="clearfix">

        <li class="about">
            <a href="about-us.php"><img src="images/footer-logo.png" alt="Mesa Posta" class="footer-logo"/></a>

            <p>Donec sollicitudin molestie malesuada. Proin eget tortor risus. Vivamus magna justo, lacinia eget
                consectetur sed, convallis at tellus. ...</p>
            <a href="about-us.php" class="readmore">Read more About Us</a>
        </li>

        <li id="recent_recipe_footer_widget-3" class="Recent_Recipe_Footer_Widget">
            <h2 class="w-bot-border">
                <span>Receitas</span> Recentes</h2>
            <ul class="recent-posts nostylewt">
                <li class="clearfix">
                    <a href="recipe-single-1.php" class="img-box">
                        <img src="images/demo/7a0a46455c4ec56a5a02c097374fc513-63x53.jpg" class="attachment-most-rated-thumb wp-post-image" alt="7a0a46455c4ec56a5a02c097374fc513"/></a>
                    <h5><a href="recipe-single-1.php">Chocolate Earl Grey Pots de...</a></h5>

                    <p>2 cups cream 120 grams dark chocolate, chopped 2 bags...</p>
                </li>
                <li class="clearfix">
                    <a href="recipe-single-1.php" class="img-box">
                        <img src="images/demo/Pesto-Pizza-with-Roasted-Garlic-Potato2-63x53.jpg" class="attachment-most-rated-thumb wp-post-image" alt="Pesto-Pizza-with-Roasted-Garlic-Potato2"/></a>
                    <h5><a href="recipe-single-1.php">Pesto Pizza With Roasted Garlic...</a></h5>
                    <p>Mention potatoes on pizza and you’ll get one of two...</p>
                </li>
            </ul>
        </li>
        <li id="displaytweetswidget-3" class="widget_displaytweetswidget">
            <h2>Twitter</h2>
            <p>Go to my twitter
                <a href="http://www.twitter.com/psk_sych3" target="_blank">@psk_sych3</a>
                <small class="muted">- Nov 12</small>
            </p>
        </li>

    </ul>
    <!-- end of bottom div -->
</div>
<!-- end of bottom-wrap div -->
<!-- ============= BOTTOM AREA ENDS HERE ============== -->


<!-- ============= FOOTER STARTS HERE ============== -->

<div id="footer-wrap">
    <div id="footer">
        <p class="copyright">Copyright &copy; 2018, Mesa Posta - PsK Sych3</p>
        <p class="dnd">Developed by <a href="http://www.twitter.com/psk_sych3">Inspiry Themes</a></p>
    </div>
    <!-- end of footer div -->
</div>
<!-- end of footer-wrapper div -->

<!-- ============= FOOTER STARTS HERE ============== -->

<script type='text/javascript' src='js/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='js/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='js/jquery.validate.min.js?ver=1.11.1'></script>
<script type='text/javascript' src='js/jquery.form.min.js?ver=3.48.0'></script>
<script type='text/javascript' src='js/jquery.easing.1.3.js?ver=1.3'></script>
<script type='text/javascript' src='js/prettyPhoto/js/jquery.prettyPhoto.js?ver=3.1.3'></script>
<script type='text/javascript' src='js/jquery.cycle2.js?ver=2.0130909'></script>
<script type='text/javascript' src='js/nivo-slider/jquery.nivo.slider.js?ver=2.7.1'></script>
<script type='text/javascript' src='js/accordion-slider.js?ver=1.0'></script>
<script type='text/javascript' src='js/jquery-ui.min.js?ver=1.10.2'></script>
<script type='text/javascript' src='js/select2.full.min.js?ver=4.0'></script>
<script type='text/javascript' src='js/script.js?ver=1.0'></script>

</body>
</html>
